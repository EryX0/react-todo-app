FROM node:14-alpine
WORKDIR /app

#copying the app files to the work directory
COPY ./ ./
#installing dependencies for the project
RUN yarn install
#bundle the app into static files for production (make it production-ready)
RUN yarn build

CMD ["yarn", "start"]